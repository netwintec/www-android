﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content.PM;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views;
using Android.Util;
using Android.Locations;
using Android.Text;

using Gcm.Client;

namespace WhatWhenWhere
{
	[Activity (Label = "MainActivity")]
	public class MainActivity : Activity,View.IOnTouchListener,GestureDetector.IOnGestureListener
	{
		int count = 1;
		public ISharedPreferences prefs;

		public bool PrimoAccesso;
		public bool Location;

		ImageView pallino1,pallino2,pallino3,pallino4,pallino5;
		ViewFlipper immagineSwipe;
		protected GestureDetector gestureScanner;
		int attivo = 1;
		private static int SWIPE_MIN_DISTANCE = 120;
		private static int SWIPE_MAX_OFF_PATH = 250;
		private static int SWIPE_THRESHOLD_VELOCITY = 200;

		public string PushToken="";

		bool notification;
		string url;

		public bool Fine = false;

		public static MainActivity Instance {get;private set;}

		protected override void OnCreate (Bundle savedInstanceState)
		{

			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);


			notification = Intent.GetBooleanExtra("Notification", false);
			url = Intent.GetStringExtra("url");

			MainActivity.Instance = this;

			prefs = Application.Context.GetSharedPreferences ("WhatWhenWhere", FileCreationMode.Private);

			ActionBar.Hide ();

			PrimoAccesso = prefs.GetBoolean("PrimoAccesso", false);
			Location = prefs.GetBoolean("LocationEnabled", false);

			GcmClient.CheckDevice(this);
			GcmClient.CheckManifest(this);
			GcmClient.Register(this, GcmBroadcastReceiver.SENDER_IDS);

			// Get our button from the layout resource,
			// and attach an event to it
			LocManager.Current.LocationServiceConnected += (object sender, ServiceConnectedEventArgs e) => {
				Console.WriteLine( "ServiceConnected Event Raised");
				// notifies us of location changes from the system
				LocManager.Current.LocationService.LocationChanged += HandleLocationChanged;
				//notifies us of user changes to the location provider (ie the user disables or enables GPS)
				LocManager.Current.LocationService.ProviderDisabled += HandleProviderDisabled;
				LocManager.Current.LocationService.ProviderEnabled += HandleProviderEnabled;
				// notifies us of the changing status of a provider (ie GPS no longer available)
				LocManager.Current.LocationService.StatusChanged += HandleStatusChanged;
			};
				

			// Start the location service:
			//LocManager.StartLocationService();

			Console.WriteLine (PrimoAccesso);

			if (PrimoAccesso == false) {

				var prefEditor = prefs.Edit();
				prefEditor.PutBoolean("WWWSport",true);
				prefEditor.PutBoolean("WWWTornei",true);
				prefEditor.PutBoolean("WWWRaduni",true);
				prefEditor.PutBoolean("WWWMostre",true);
				prefEditor.PutBoolean("WWWSfilate",true);
				prefEditor.PutBoolean("WWWVita",true);
				prefEditor.PutBoolean("WWWAperitivi",true);
				prefEditor.PutBoolean("WWWGay",true);
				prefEditor.PutBoolean("WWWOtaku",true);
				prefEditor.PutBoolean("WWWConcerti",true);
				prefEditor.PutBoolean("WWWMercatini",true);
				prefEditor.PutBoolean("WWWFiere",true);
				prefEditor.PutBoolean("WWWSagre",true);
				prefEditor.PutBoolean("WWWInaugurazione",true);
				prefEditor.PutBoolean("WWWCinema",true);
				prefEditor.PutBoolean("WWWTeatro",true);
				prefEditor.PutBoolean("WWWOnlus",true);
				prefEditor.PutBoolean("WWWReligione",true);
				prefEditor.PutBoolean("WWWBambini",true);
				prefEditor.PutBoolean("WWWItinerari",true);
				prefEditor.PutBoolean("WWWCorsi",true);
				prefEditor.PutBoolean("WWWPromo",true);
				prefEditor.PutBoolean("WWWScioperi",true);
				prefEditor.PutBoolean("WWWPropaganda",true);
				prefEditor.Commit();


				FindViewById<RelativeLayout> (Resource.Id.black).Visibility = ViewStates.Gone;

				var button = FindViewById<Button> (Resource.Id.button1);
				button.Click += delegate {
					
					FindViewById<RelativeLayout> (Resource.Id.black).Visibility = ViewStates.Visible;

					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.SetTitle("Posizione");
					builder.SetMessage("Vuoi che l'app usi la tua posizione.\nQuesta opzione sara modificabile anche dal menù Impostazioni.")
						.SetCancelable(true).SetPositiveButton("Acconsenti",delegate {

							var prefs = MainActivity.Instance.prefs;
							var prefEditor2 = prefs.Edit();
							prefEditor2.PutBoolean("PrimoAccesso",true);
							prefEditor2.Commit();

							LocManager.StartLocationService();
							Location = true;
							var prefEditor3= prefs.Edit();
							prefEditor3.PutBoolean("LocationEnabled",Location);
							prefEditor3.Commit();
							var intent = new Intent (this, typeof(HomePageActivity));
							StartActivity (intent);
							//Finish();
						}
						).SetNegativeButton("Non Acconsentire", delegate {

							var prefs = MainActivity.Instance.prefs;
							var prefEditor2 = prefs.Edit();
							prefEditor2.PutBoolean("PrimoAccesso",true);
							prefEditor2.Commit();

							//LocManager.StopLocationService();
							Location = false;
							var prefEditor3 = prefs.Edit();
							prefEditor3.PutBoolean("LocationEnabled",Location);
							prefEditor3.Commit();
							var intent = new Intent (this, typeof(HomePageActivity));
							StartActivity (intent);
							//Finish();
						});
					
					AlertDialog alert = builder.Create();
					alert.Show();

				};

				immagineSwipe = FindViewById<ViewFlipper> (Resource.Id.immagineSwipe);
				immagineSwipe.SetOnTouchListener (this);

				pallino1 = FindViewById<ImageView> (Resource.Id.pallino1);
				pallino2 = FindViewById<ImageView> (Resource.Id.pallino2);
				pallino3 = FindViewById<ImageView> (Resource.Id.pallino3);
				pallino4 = FindViewById<ImageView> (Resource.Id.pallino4);
				pallino5 = FindViewById<ImageView> (Resource.Id.pallino5);

				gestureScanner = new GestureDetector(this);

			} else {

				if (Location)
					LocManager.StartLocationService();


				if (notification)
				{
					var intent = new Intent(this, typeof(NotificationActivity));
					intent.PutExtra("url", url);
					intent.PutExtra("Notification", true);
					StartActivity(intent);
					//Finish();
				}
				else {
					var intent = new Intent(this, typeof(HomePageActivity));
					StartActivity(intent);
					//Finish();
				}
			}

			//string prova1 = "<p><img src=\"http://www.cortexa.it/images/articoli/formazione/corsi%20online%202016/corso%201.jpg\" alt=\"corso 3\"  /></p>";
			//string prova2 = "<div class=\"articolo_immagine\"><img src=\"http://www.cortexa.it/images/articoli/formazione/corsi%20online%202016/CORSO_01_POSA_PROGETTAZIONE_01.jpg\" alt=\"CORSO 01 POSA PROGETTAZIONE 01\" width=\"268\" class=\"thumb\" style=\"margin-left: 20px; float: right;\" /></div><h3><strong><strong>Venerd&igrave; 6 Maggio 2016 &ndash; ore 14.00 / 15.30</strong></strong></h3><p><strong>&nbsp;</strong></p><h4><strong>Titolo: <br />CORRETTA POSA DEL SISTEMA A CAPPOTTO - PROGETTAZIONE<br /></strong></h4><p><strong>&nbsp;</strong></p><p><strong>&nbsp;</strong></p><h3><strong>&nbsp;</strong></h3><p><strong>&nbsp;</strong></p><p>Caro utente,<br /><br />vogliamo ricordarti che <strong>per assistere al corso online </strong>&ldquo;<strong>CORRETTA POSA DEL SISTEMA A CAPPOTTO</strong>&rdquo; ti baster&agrave; semplicemente <strong>cliccare sul link che riceverai nella tua posta elettronica</strong>qualche giorno prima del webinar per poterti cos&igrave; collegare direttamente all&rsquo;interno dell&rsquo;aula digitale dove si svolger&agrave; il corso. Ti informiamo inoltre che durante lo svolgersi del corso, della durata di un&rsquo;ora e mezza,avrai anche la <strong>possibilit&agrave; di intervenire con domande e proposte di argomenti</strong> per te pi&ugrave; importanti, utilizzando la chat messa a disposizione per gli utenti, usufruendo cos&igrave; di risposte immediate e pertinenti aituoi quesiti.</p><p>Ti ricordiamo che per una fruizione ottimale dell&rsquo;evento,<strong> &egrave; necessaria una buona qualit&agrave; della connessione Internet</strong> e ti preghiamo quindi di <strong>controllare qui di seguito i requisiti minimi richiesti</strong>.</p><p>Non avrai bisogno di ulteriori programmi o strumenti come webcam o microfono, ma baster&agrave; una buona resa dell&rsquo;audio del tuo computer o casse audio adeguate.</p><p>Ti auguriamo buon lavoro!</p><p><em>Lo staff di Cortexa</em></p><p><em>&nbsp;</em></p><h3 class=\"Corpo\"><strong>Requisiti minimi accessibilit&agrave; Computer</strong><span style=\"font-size: 8pt;\"> </span></h3><p><span style=\"text-decoration: underline;\"><strong><br />Caratteristiche computer</strong> </span><br />Processore 3.0 GHz -512 MB RAM <br />Risoluzione schermo 1024x768 <br />Scheda audio</p><p><span style=\"text-decoration: underline;\"><strong>Connettivit&agrave; (DSL/Fibra/HSDPA)</strong></span><br />Download: 512 kb/s effettivi (consigliati 1024 kb/s) <br />Upload: 256 kb/s effettivi (consigliati 512 kb/s)</p><p><span style=\"text-decoration: underline;\"><strong>Browser Windows</strong></span><br />(XP, Vista, 7) <br />Explorer 7 o succ. <br />Firefox 3.5 o succ. <br />Safari 4.0.4 o succ. <br />Opera 9.6 o succ.</p><p><span style=\"text-decoration: underline;\"><strong>Browser Mac</strong></span><br />(10.4 o succ.) <br />Firefox 3.0 o succ. <br />Safari 4.0.4 o succ.</p><p><span style=\"text-decoration: underline;\"><strong>Browser Linux</strong></span><br />Firefox 3.0 o succ.</p><p><span style=\"text-decoration: underline;\"><strong>Player</strong></span><br />Adobe Flash Player 9.0.115.0 o succ.</p><p><span style=\"text-decoration: underline;\"><strong>Accessori multimediali</strong></span><br />casse audio o impianto audio incorporato</p><h4>Note <br />Durante la sessione &egrave; consigliabile terminare l'esecuzione di altri applicativi che potrebbero sottrarre risorse al sistema.</h4>";
			//string prova3 = "<p><span style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\"><img src=\"images/cortexa-sale-in-cattedra.jpg\" alt=\"\" style=\"max-width: 100%;\" /></span></p><p>&nbsp;</p><p style=\"text-align: justify;\"><span style=\"color: #1b2432; font-family: helvetica; line-height: 20px; white-space: pre-wrap;\">Il Consorzio Cortexa&nbsp;&nbsp;&egrave; stato <strong>protagonista di un percorso formativo presso&nbsp;&nbsp;il corso di Laurea Magistrale dedicato alla progettazione presso la Facolt&agrave; di Ingegneria dell'Universit&agrave; di Firenze</strong>. Con questa iniziativa &egrave; stato possibile per il Consorzio discutere insieme ai ragazzi in merito ad importanti tematiche relative al <strong>Sistema a Cappotto.</strong></span><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><span style=\"font-family: helvetica;\"><span style=\"color: #1b2432; line-height: 20px; white-space: pre-wrap;\">Cortexa ha ten</span><span style=\"color: #1b2432; line-height: 20px; white-space: pre-wrap;\">uto 3 lezioni a partire dal 3 marzo dedicate ai seguenti temi:</span></span><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><span style=\"color: #1b2432; font-family: helvetica; line-height: 20px; white-space: pre-wrap;\"> 1) <strong>Prima lezione 3 marzo - dedicata alla progettazione -</strong> progettazione esecutiva del Sistema a Cappotto, dettagli costruttivi ed errori da evitare;</span><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><span style=\"color: #1b2432; font-family: helvetica; line-height: 20px; white-space: pre-wrap;\"> 2) <strong>Seconda lezione 10 marzo - dedicata alle prestazioni e materiali</strong> - sollecitazioni meccaniche e guida alla scelta dei materiali isolanti (prima parte);</span><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><span style=\"color: #1b2432; font-family: helvetica; line-height: 20px; white-space: pre-wrap;\"> 3) <strong>Terza lezione 17 marzo - protezione acustica con il Sistema a Cappotto</strong></span><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><br style=\"color: #1b2432; font-family: proxima-nova, 'Helvetica Neue', Arial, sans-serif; line-height: 20px; white-space: pre-wrap;\" /><span style=\"color: #1b2432; font-family: helvetica; line-height: 20px; white-space: pre-wrap;\">Considerato il positivo esito di questa iniziativa, per <strong>il prossimo anno il Consorzio proseguir&agrave; con la sua missione</strong> in un nuovo Master dedicato alla sostenibilit&agrave; ambientale!</span></p><p style=\"text-align: justify;\">&nbsp;</p><p style=\"text-align: justify;\">&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";

			//var label = FindViewById<TextView> (Resource.Id.textView1);
			//label.TextFormatted=Html.FromHtml(prova3);


			//var intent = new Intent (this, typeof(HomePageActivity));
			//StartActivity (intent);


		}

		protected override void OnPause()
		{
			Console.WriteLine( "OnPause: Location app is moving to background");
			base.OnPause();
		}


		protected override void OnResume()
		{
			Console.WriteLine( "OnResume: Location app is moving into foreground");

			if (MainActivity.Instance != null) { 
				if(MainActivity.Instance.Fine){
					Finish();
				}
			}

			base.OnResume();
		}

		protected override void OnDestroy ()
		{
			Console.WriteLine( "OnDestroy: Location app is becoming inactive");
			base.OnDestroy ();

			// Stop the location service:
			LocManager.StopLocationService();
		}

		public bool OnTouch (View v, MotionEvent e)
		{
			return gestureScanner.OnTouchEvent(e);
		}

		public bool OnDown (MotionEvent e)
		{
			return true;
		}

		public bool OnFling (MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			Console.WriteLine(e1.GetX()+"  "+e2.GetX()+"  "+ Math.Abs(e1.GetX() - e2.GetX())+"  "+velocityX+"  "+Math.Abs(velocityX));
			try {
				if(e1.GetX() > e2.GetX() && Math.Abs(e1.GetX() - e2.GetX()) > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					Console.WriteLine("L");
					//Toast.makeText(this.getApplicationContext(), "Left", Toast.LENGTH_SHORT).show();
					attivo++;
					if(attivo!=6){
						immagineSwipe.ShowNext();
						cambiaPallino(attivo);
					}
					else{
						attivo--;
					}
				}else if (e1.GetX() < e2.GetX() && e2.GetX() - e1.GetX() > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					Console.WriteLine("R");
					//Toast.makeText(this.getApplicationContext(), "Right", Toast.LENGTH_SHORT).show();
					attivo--;
					if(attivo!=0){
						immagineSwipe.ShowPrevious();
						cambiaPallino(attivo);
					}
					else{
						attivo++;
					}
				}
			} catch (Exception e) {
				// nothing
			}
			return true;

		}

		public void OnLongPress (MotionEvent e)
		{
		}

		public bool OnScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			return true;
		}

		public void OnShowPress (MotionEvent e)
		{
		}

		public bool OnSingleTapUp (MotionEvent e)
		{
			return true;
		}

		public void cambiaPallino(int posizione){

			if (posizione == 1) {
				pallino1.SetImageResource (Resource.Drawable.pallinogiallo);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
				pallino4.SetImageResource (Resource.Drawable.pallinobianco);
				pallino5.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 2) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinogiallo);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
				pallino4.SetImageResource (Resource.Drawable.pallinobianco);
				pallino5.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 3) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinogiallo);
				pallino4.SetImageResource (Resource.Drawable.pallinobianco);
				pallino5.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 4) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
				pallino4.SetImageResource (Resource.Drawable.pallinogiallo);
				pallino5.SetImageResource (Resource.Drawable.pallinobianco);
			}
			if (posizione == 5) {
				pallino1.SetImageResource (Resource.Drawable.pallinobianco);
				pallino2.SetImageResource (Resource.Drawable.pallinobianco);
				pallino3.SetImageResource (Resource.Drawable.pallinobianco);
				pallino4.SetImageResource (Resource.Drawable.pallinobianco);
				pallino5.SetImageResource (Resource.Drawable.pallinogiallo);
			}
		}

		public void HandleLocationChanged(object sender, LocationChangedEventArgs e)
		{
			Android.Locations.Location location = e.Location;
			//Console.WriteLine( "Foreground updating");

			// these events are on a background thread, need to update on the UI thread
			/*RunOnUiThread (() => {
				latText.Text = String.Format ("Latitude: {0}", location.Latitude);
				longText.Text = String.Format ("Longitude: {0}", location.Longitude);
				altText.Text = String.Format ("Altitude: {0}", location.Altitude);
				speedText.Text = String.Format ("Speed: {0}", location.Speed);
				accText.Text = String.Format ("Accuracy: {0}", location.Accuracy);
				bearText.Text = String.Format ("Bearing: {0}", location.Bearing);
			});*/

		}

		public void HandleProviderDisabled(object sender, ProviderDisabledEventArgs e)
		{
			Console.WriteLine(  "Location provider disabled event raised");
		}

		public void HandleProviderEnabled(object sender, ProviderEnabledEventArgs e)
		{
			Console.WriteLine(  "Location provider enabled event raised");
		}

		public void HandleStatusChanged(object sender, StatusChangedEventArgs e)
		{
			Console.WriteLine(  "Location status changed, event raised");
		}

	}
}


