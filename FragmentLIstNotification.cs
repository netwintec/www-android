﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Json;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Content.PM;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using RestSharp;

namespace WhatWhenWhere
{
	[Activity(Label = "FragmentLIstNotification", Theme = "@style/AppTheme", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize)]
	public class FragmentLIstNotification : BaseActivity
	{

		public ImageView menuIcon;
		public ImageView backIcon;
		public ImageView notIcon;
		public ImageView relIcon;

		ListView list;
		List<ListNot> l = new List<ListNot>();
		ListNotAdapter Adapter;

		public RelativeLayout LoadL, ListL,TextL;


		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			setActionBarIcon(Resource.Drawable.settings, Resource.Drawable.back_icon, Resource.Drawable.notifica, Resource.Drawable.reload);


			menuIcon = (ImageView)getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView)getToolbar().FindViewById(Resource.Id.back_icon);
			notIcon = (ImageView)getToolbar().FindViewById(Resource.Id.notification_icon);
			relIcon = (ImageView)getToolbar().FindViewById(Resource.Id.reload_icon);

			relIcon.Visibility = ViewStates.Invisible;
			notIcon.Visibility = ViewStates.Invisible;

			menuIcon.Click += delegate
			{
				var intent = new Intent(this, typeof(FragmentSettings));
				StartActivity(intent);
				Finish();

			};

			backIcon.Click += delegate
			{

				var intent = new Intent(this, typeof(HomePageActivity));
				StartActivity(intent);
				Finish();

			};

			Typeface tf = Typeface.CreateFromAsset(Assets, "fonts/Lato.ttf");

			HomePageActivity.Instance.LastPg = 2;

			FindViewById<ProgressBar>(Resource.Id.progressBar1).IndeterminateDrawable.SetColorFilter(Color.Rgb(255,204,0),PorterDuff.Mode.Multiply);
			FindViewById<TextView>(Resource.Id.textView1).Typeface =tf;

			LoadL = FindViewById<RelativeLayout>(Resource.Id.LoadLayout);
			ListL = FindViewById<RelativeLayout>(Resource.Id.ListLayout);
			TextL = FindViewById<RelativeLayout>(Resource.Id.TextLayout);

			LoadL.Visibility = ViewStates.Visible;
			ListL.Visibility = ViewStates.Gone;
			TextL.Visibility = ViewStates.Gone;
			/*
			l.Add(new ListNot("Not 1", "http://www.whatwhenwhere.it/event.php?e=4382"));
			l.Add(new ListNot("Not 2", "http://www.whatwhenwhere.it/event.php?e=5339"));
			l.Add(new ListNot("Not 3", "http://www.whatwhenwhere.it/event.php?e=1200"));
			l.Add(new ListNot("Not 4", "http://www.whatwhenwhere.it/event.php?e=3450"));
			l.Add(new ListNot("Not 5", "http://www.whatwhenwhere.it/event.php?e=5000"));
			*/

			Adapter = new ListNotAdapter(this,l);

			list = FindViewById<ListView>(Resource.Id.listView1);
			list.Adapter = Adapter; 
			list.SetFooterDividersEnabled(false);
			list.SetHeaderDividersEnabled(false);
			list.Divider.SetAlpha(0);
			list.ItemClick += ChatListItemClick;

			LoadData();

		}

		public void ChatListItemClick(object sender, AdapterView.ItemClickEventArgs item)
		{
			selectItem(item.Position);
		}

		public void selectItem(int position)
		{
			Console.WriteLine(position);
			list.SetItemChecked(position, false);

			var intent = ApplicationContext.PackageManager.GetLaunchIntentForPackage(ApplicationContext.PackageName);
			intent = new Intent(this, typeof(NotificationActivity));


			intent.PutExtra("url", l[position].Url);

			StartActivity(intent);
		}

		//**** LOAD NOTIFICATION DATA ****/
		public void LoadData() { 
		
			var client = new RestClient("http://api.whatwhenwhere.it/");

			var request = new RestRequest("devices/" + Android.OS.Build.Serial + "/notifications?os=Android", Method.GET);

			foreach (var p in request.Parameters)
			{
				Console.WriteLine(p.Name + "|" + p.ContentType + "|" + p.Value + "|" + p.Type);
			}

			client.ExecuteAsync(request, (s, e) =>
			{
				 
				Console.WriteLine("Result ListNOT:" + s.StatusCode + "|" + s.Content);

				if (s.StatusCode == System.Net.HttpStatusCode.OK)
				{
					RunOnUiThread(() =>
					{
						List<ListNot> listAppoggio = new List<ListNot>();

						JsonValue json = JsonValue.Parse(s.Content);

						foreach (JsonValue j in json)
						{

							string url = j["url"];
							string name = j["name"];
							string img = j["image"];

							JsonValue jj = j["category"]; ;
							string category = jj["name"];

							listAppoggio.Add(new ListNot(name, url, img,category));
						}

						if (listAppoggio.Count == 0)
						{
							LoadL.Visibility = ViewStates.Gone;
							ListL.Visibility = ViewStates.Gone;
							TextL.Visibility = ViewStates.Visible;
						}
						else {
							for (int i = listAppoggio.Count - 1; i >= 0; i--)
							{

								l.Add(listAppoggio[i]);
								//list.Add (listAppoggio [i]);
								//Enumerable.Reverse (list);
							}


							//adapter.listChat.Add(new Message(messageT,false));
							Adapter.NotifyDataSetChanged();

							LoadL.Visibility = ViewStates.Gone;
							ListL.Visibility = ViewStates.Visible;
						}
					});

				}
				else {
					LoadL.Visibility = ViewStates.Gone;
					ListL.Visibility = ViewStates.Gone;
					TextL.Visibility = ViewStates.Visible;
				}
			});
		}

		public override void OnBackPressed()
		{

			var intent = new Intent(this, typeof(HomePageActivity));
			StartActivity(intent);
			Finish();

		}

		public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
		}

		protected override int getLayoutResource()
		{
			return Resource.Layout.FragmentListNot;
		}
	}
}

