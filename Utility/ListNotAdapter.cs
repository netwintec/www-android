﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;

namespace WhatWhenWhere
{
	public class ListNotAdapter : BaseAdapter
	{
		FragmentLIstNotification super;
		public List<ListNot> listNot;

		public ListNotAdapter(FragmentLIstNotification context, List<ListNot> lc) : base()
		{
			super = context;
			listNot = lc;
		}

		public override int Count
		{
			get { return listNot.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  
			Typeface tf = Typeface.CreateFromAsset(super.Assets, "fonts/Lato.ttf");

			View view = convertView;

			LayoutInflater inflater = ((Activity)super
			                          ).LayoutInflater;

			view = inflater.Inflate(Resource.Layout.NotCell, parent, false);

			ListNot Item = this.listNot[position];


			view.FindViewById<TextView>(Resource.Id.textView1).Typeface = tf;
			view.FindViewById<TextView>(Resource.Id.textView1).Text = Item.Nome;
			view.FindViewById<TextView>(Resource.Id.textView2).Typeface = tf;
			view.FindViewById<TextView>(Resource.Id.textView2).Text = Item.Category;

			if(Item.ImgBtm == null)
				caricaImmagineAsync(Item.Img,view.FindViewById<ImageView>(Resource.Id.imageView1),Item);
			else
				view.FindViewById<ImageView>(Resource.Id.imageView1).SetImageBitmap(Item.ImgBtm);

			return view;
		}

		public ListNot GetItemAtPosition(int position)
		{
			return listNot[position];
		}

		async void caricaImmagineAsync(String uri, ImageView immagine_view,ListNot obj)
		{
			WebClient webClient = new WebClient();
			byte[] bytes = null;
			try
			{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch (TaskCanceledException)
			{
				Console.WriteLine("Task Canceled!**************************************");
				return;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap(immagine);
			obj.ImgBtm = immagine;

			Console.WriteLine("Immagine caricata!");

		}
	}
}

