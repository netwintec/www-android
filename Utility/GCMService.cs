﻿using System.Text;
using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Util;
using Android.Graphics;
using Gcm.Client;
using RestSharp;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

[assembly: UsesPermission (Android.Manifest.Permission.ReceiveBootCompleted)]

namespace WhatWhenWhere
{
	//You must subclass this!
	[BroadcastReceiver(Permission=Constants.PERMISSION_GCM_INTENTS)]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new string[] { "@PACKAGE_NAME@" })]

	public class GcmBroadcastReceiver : GcmBroadcastReceiverBase<PushHandlerService>
	{
		//IMPORTANT: Change this to your own Sender ID!
		//The SENDER_ID is your Google API Console App Project ID.
		//  Be sure to get the right Project ID from your Google APIs Console.  It's not the named project ID that appears in the Overview,
		//  but instead the numeric project id in the url: eg: https://code.google.com/apis/console/?pli=1#project:762855674567:overview
		//  where 785671162406 is the project id, which is the SENDER_ID to use!
		public static string[] SENDER_IDS = new string[] {"539571024206"};

		public const string TAG = "PushSharp-GCM";
	}

	[Service] //Must use the service tag
	public class PushHandlerService : GcmServiceBase
	{
		public PushHandlerService() : base(GcmBroadcastReceiver.SENDER_IDS) { }

		const string TAG = "GCM-SAMPLE";

		bool SportNot, TorneiNot, RaduniNot, MostreNot, SfilateNot;
		bool VitaNot, AperitiviNot, GayNot, OtakuNot, ConcertiNot;
		bool MercatiniNot, FiereNot, SagreNot, InaugurazioniNot, CinemaNot;
		bool TeatroNot, OnlusNot, ReligioneNot, BambiniNot, ItinerariNot;
		bool CorsiNot, PromoNot, ScioperiNot, PropagandaNot;

		ISharedPreferences prefs;

		protected override void OnRegistered (Context context, string registrationId)
		{
			Log.Verbose(TAG, "GCM Registered: " + registrationId);
			//Eg: Send back to the server
			//	var result = wc.UploadString("http://your.server.com/api/register/", "POST", 
			//		"{ 'registrationId' : '" + registrationId + "' }");

			MainActivity.Instance.PushToken = registrationId;

			if (MainActivity.Instance.PrimoAccesso == false)
			{
				MainActivity.Instance.RunOnUiThread(() =>
				{

					// Invio Dati Location
					Console.WriteLine("NOtification Send at http://api.whatwhenwhere.it/devices/" + Android.OS.Build.Serial + "/position");
					var client = new RestClient("http://api.whatwhenwhere.it/");

					var request = new RestRequest("devices/" + Android.OS.Build.Serial + "/categories", Method.POST);
					request.AddHeader("Content-Type", "multipart/form-data");

					request.AddParameter("os", "Android");
					request.AddParameter("token", registrationId);

					request.AddParameter("categories[]", "sport");
					request.AddParameter("categories[]", "tournaments");
					request.AddParameter("categories[]", "gatherings");
					request.AddParameter("categories[]", "exhibitions");
					request.AddParameter("categories[]", "parades");
					request.AddParameter("categories[]", "nightlife");
					request.AddParameter("categories[]", "aperitives");
					request.AddParameter("categories[]", "gay-lgbt");
					request.AddParameter("categories[]", "otaku");
					request.AddParameter("categories[]", "concerts");
					request.AddParameter("categories[]", "markets");
					request.AddParameter("categories[]", "fairs");
					request.AddParameter("categories[]", "festivals");
					request.AddParameter("categories[]", "inaugurations");
					request.AddParameter("categories[]", "cinema");
					request.AddParameter("categories[]", "theatre");
					request.AddParameter("categories[]", "onlus");
					request.AddParameter("categories[]", "religion");
					request.AddParameter("categories[]", "kids");
					request.AddParameter("categories[]", "routes");
					request.AddParameter("categories[]", "training-courses");
					request.AddParameter("categories[]", "promos-discounts");
					request.AddParameter("categories[]", "strike");
					request.AddParameter("categories[]", "propaganda");

					//requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


					client.ExecuteAsync(request, (s, e) =>
					{

						Console.WriteLine("Result:" + s.StatusCode + "|" + s.Content);

					});

					Console.WriteLine(1);

				});

				//createNotification("GCM Registered...", "The device has been Registered, Tap to View!");
			}
			else {


				prefs = MainActivity.Instance.prefs;

				SportNot = prefs.GetBoolean("WWWSport", false);
				TorneiNot = prefs.GetBoolean("WWWTornei", false);
				RaduniNot = prefs.GetBoolean("WWWRaduni", false);
				MostreNot = prefs.GetBoolean("WWWMostre", false);
				SfilateNot = prefs.GetBoolean("WWWSfilate", false);
				VitaNot = prefs.GetBoolean("WWWVita", false);
				AperitiviNot = prefs.GetBoolean("WWWAperitivi", false);
				GayNot = prefs.GetBoolean("WWWGay", false);
				OtakuNot = prefs.GetBoolean("WWWOtaku", false);
				ConcertiNot = prefs.GetBoolean("WWWConcerti", false);
				MercatiniNot = prefs.GetBoolean("WWWMercatini", false);
				FiereNot = prefs.GetBoolean("WWWFiere", false);
				SagreNot = prefs.GetBoolean("WWWSagre", false);
				InaugurazioniNot = prefs.GetBoolean("WWWInaugurazione", false);
				CinemaNot = prefs.GetBoolean("WWWCinema", false);
				TeatroNot = prefs.GetBoolean("WWWTeatro", false);
				OnlusNot = prefs.GetBoolean("WWWOnlus", false);
				ReligioneNot = prefs.GetBoolean("WWWReligione", false);
				BambiniNot = prefs.GetBoolean("WWWBambini", false);
				ItinerariNot = prefs.GetBoolean("WWWItinerari", false);
				CorsiNot = prefs.GetBoolean("WWWCorsi", false);
				PromoNot = prefs.GetBoolean("WWWPromo", false);
				ScioperiNot = prefs.GetBoolean("WWWScioperi", false);
				PropagandaNot = prefs.GetBoolean("WWWPropaganda", false);

				MainActivity.Instance.RunOnUiThread(() =>
				{

					// Invio Dati Location
					Console.WriteLine("Notification Send at http://api.whatwhenwhere.it/devices/" + Android.OS.Build.Serial + "/position");
					var client = new RestClient("http://api.whatwhenwhere.it/");

					var request = new RestRequest("devices/" + Android.OS.Build.Serial + "/categories", Method.POST);
					request.AddHeader("Content-Type", "multipart/form-data");

					request.AddParameter("os", "Android");
					request.AddParameter("token", registrationId);

					if (SportNot)
						request.AddParameter("categories[]", "sport");
					if (TorneiNot)
						request.AddParameter("categories[]", "tournaments");
					if (RaduniNot)
						request.AddParameter("categories[]", "gatherings");
					if (MostreNot)
						request.AddParameter("categories[]", "exhibitions");
					if (SfilateNot)
						request.AddParameter("categories[]", "parades");
					if (VitaNot)
						request.AddParameter("categories[]", "nightlife");
					if (AperitiviNot)
						request.AddParameter("categories[]", "aperitives");
					if (GayNot)
						request.AddParameter("categories[]", "gay-lgbt");
					if (OtakuNot)
						request.AddParameter("categories[]", "otaku");
					if (ConcertiNot)
						request.AddParameter("categories[]", "concerts");
					if (MercatiniNot)
						request.AddParameter("categories[]", "markets");
					if (FiereNot)
						request.AddParameter("categories[]", "fairs");
					if (SagreNot)
						request.AddParameter("categories[]", "festivals");
					if (InaugurazioniNot)
						request.AddParameter("categories[]", "inaugurations");
					if (CinemaNot)
						request.AddParameter("categories[]", "cinema");
					if (TeatroNot)
						request.AddParameter("categories[]", "theatre");
					if (OnlusNot)
						request.AddParameter("categories[]", "onlus");
					if (ReligioneNot)
						request.AddParameter("categories[]", "religion");
					if (BambiniNot)
						request.AddParameter("categories[]", "kids");
					if (ItinerariNot)
						request.AddParameter("categories[]", "routes");
					if (CorsiNot)
						request.AddParameter("categories[]", "training-courses");
					if (PromoNot)
						request.AddParameter("categories[]", "promos-discounts");
					if (ScioperiNot)
						request.AddParameter("categories[]", "strike");
					if (PropagandaNot)
						request.AddParameter("categories[]", "propaganda");

					//requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

					foreach (var p in request.Parameters)
					{
						Console.WriteLine(p.Name + "|" + p.ContentType + "|" + p.Value + "|" + p.Type);
					}

					client.ExecuteAsync(request, (s, e) =>
					{

						Console.WriteLine("Result NOT:" + s.StatusCode + "|" + s.Content);

					});

					Console.WriteLine(1);

				});

			}
		}

		protected override void OnUnRegistered (Context context, string registrationId)
		{
			Log.Verbose(TAG, "GCM Unregistered: " + registrationId);
			//Remove from the web service
			//	var wc = new WebClient();
			//	var result = wc.UploadString("http://your.server.com/api/unregister/", "POST",
			//		"{ 'registrationId' : '" + lastRegistrationId + "' }");

			//createNotification("GCM Unregistered...", "The device has been unregistered, Tap to View!");
		}

		protected override void OnMessage (Context context, Intent intent)
		{
			Log.Info(TAG, "GCM Message Received!");

			var msg = new StringBuilder();  

			if (intent != null && intent.Extras != null)
			{
				foreach (var key in intent.Extras.KeySet())
					msg.AppendLine(key + "=" + intent.Extras.Get(key).ToString());
			}

			Console.WriteLine ("message:\n"+msg);





			string url;
			try{
				url = intent.Extras.Get("url").ToString();
			}catch(Exception e){
				url = "";
			}

			string name;
			try
			{
				name = intent.Extras.Get("name").ToString();
			}
			catch (Exception e)
			{
				name = "";
			}


			Console.WriteLine ("1:"+url+"|"+name);
			bool close2;
			try{
				close2 = MainActivity.Instance.IsFinishing;
			}catch(Exception e){
				close2 = true;
			}

			//MainActivity.Instance.Notification

			Console.WriteLine ("2:"+close2);

			createNotification(url,close2,name);
		}

		protected override bool OnRecoverableError (Context context, string errorId)
		{
			Log.Warn(TAG, "Recoverable Error: " + errorId);
			return base.OnRecoverableError (context, errorId);
		}

		protected override void OnError (Context context, string errorId)
		{
			Log.Error(TAG, "GCM Error: " + errorId);
		}

		void createNotification(string url,bool close2,string name)
		{


			Console.WriteLine ("cN:"+url+"|"+close2+"|"+name);

			var manager =
				(NotificationManager)this.ApplicationContext.GetSystemService(Context.NotificationService);

			Intent intent;

			if (close2) {

				var intent2 = ApplicationContext.PackageManager.GetLaunchIntentForPackage(this.ApplicationContext.PackageName);
				intent = new Intent (this, typeof(SplashScreen));
				intent.PutExtra ("url",url);
				intent.PutExtra ("Notification",true);

			} else {

				var intent2 = ApplicationContext.PackageManager.GetLaunchIntentForPackage(this.ApplicationContext.PackageName);
				intent = new Intent(this, typeof(NotificationActivity));

				intent.PutExtra("url", url);
					

			}

			var pendingIntent = PendingIntent.GetActivity(this.ApplicationContext, 0, intent, PendingIntentFlags.UpdateCurrent);

			//Bitmap largeIcon = BitmapFactory.DecodeResource(Resources , Resource.Drawable.header);

			var builder = new Notification.Builder(this.ApplicationContext)
				.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
				.SetSmallIcon(Resource.Mipmap.notification)
				.SetContentTitle("WhatWhewWhere")
				.SetContentText(name)//"Un nuovo evento che ti potrebbe interessare è stato aggiunto")
				.SetContentIntent(pendingIntent)
				.SetAutoCancel(true);



			manager.Notify(0,builder.Build());

		}
	}
}

