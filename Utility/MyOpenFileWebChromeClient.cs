﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.OS;
using Android.Webkit;
using Android.Views;

namespace WhatWhenWhere
{
	class MyOpenFileWebChromeClient : OpenFileWebChromeClient
	{

		Action<IValueCallback> cb;

		public MyOpenFileWebChromeClient(Action<IValueCallback> cb)
		{
			this.cb = cb;
		}

		public override void OpenFileChooser(IValueCallback uploadMsg)
		{
			cb(uploadMsg);
		}

		public override void OnProgressChanged(WebView view, int progress)
		{
			//Console.WriteLine (progress);

			if (progress == 100)
			{
				FragmentHome.Instance.bar.Progress = 0;
				FragmentHome.Instance.bar.Visibility = ViewStates.Gone;
			}
			else {
				FragmentHome.Instance.bar.Progress = progress;
				FragmentHome.Instance.bar.Visibility = ViewStates.Visible;
			}

		}

		public override void OnGeolocationPermissionsShowPrompt(string origin, GeolocationPermissions.ICallback callback)
		{
			System.Console.WriteLine("SHOW");
			callback.Invoke(origin, MainActivity.Instance.Location, false);
		}
	}
}

