﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content.PM;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Webkit;


using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace WhatWhenWhere
{			
	[Activity (Label = "HomePage",Theme = "@style/AppTheme",ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize/*,ScreenOrientation = ScreenOrientation.Portrait*/)]			
	public class HomePageActivity : BaseActivity 
	{

		public ImageView menuIcon;
		public ImageView backIcon;
		public ImageView notIcon;
		public ImageView relIcon;

		public WebView webview;
		public ProgressBar bar;

		bool isSetting = false;
		bool isListNot = false;
		bool NotificationOption = false;
		bool NotificationNot = false;

		public int LastPg = 0;

		IValueCallback mUploadMessage;
		private static int FILECHOOSER_RESULTCODE = 1;

		public String mCM;
		public IValueCallback mUM;
		public IValueCallback mUMA;

		public string Lasturl = "http://www.whatwhenwhere.it/";
		public int BackStep = 0;

		public static HomePageActivity Instance { private set; get;}

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			setActionBarIcon(Resource.Drawable.settings, Resource.Drawable.back_icon, Resource.Drawable.notifica,Resource.Drawable.reload);

			try
			{
				LastPg = HomePageActivity.Instance.LastPg;
			}
			catch (Exception e) { }

			HomePageActivity.Instance = this;

			//Set default settings
			//PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

			if (Intent.HasExtra("Option"))
				NotificationOption = Intent.GetBooleanExtra("Option", false);
			if (Intent.HasExtra("Not"))
				NotificationNot = Intent.GetBooleanExtra("Not", false);
			//mDrawerList.Adapter = new ArrayAdapter<string> (this, Resource.Layout.item_menu, Section);ì

			menuIcon = (ImageView)getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView)getToolbar().FindViewById(Resource.Id.back_icon);
			notIcon = (ImageView)getToolbar().FindViewById(Resource.Id.notification_icon);
			relIcon = (ImageView)getToolbar().FindViewById(Resource.Id.reload_icon);

			backIcon.Visibility = ViewStates.Invisible;

			relIcon.Click += delegate
			 {
				 if (!isSetting && !isListNot)
				 {
					BackStep++;
					webview.LoadUrl(Lasturl);
				 }
 
			 };

			notIcon.Click += delegate
			{

				var intent = new Intent(this, typeof(FragmentLIstNotification));
				StartActivity(intent);
				Finish();

				isSetting = false;
				isListNot = true;

			};

			menuIcon.Click += delegate
			{
				var intent = new Intent(this, typeof(FragmentSettings));
				StartActivity(intent);
				Finish();

				isSetting = true;
				isListNot = false;

			};
			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

			backIcon.Click += delegate
			{

				if (webview.CanGoBack())
				{
					/*if (BackStep != 0)
					{
						Console.WriteLine(BackStep);
						webview.GoBackOrForward(-1 - BackStep);
						BackStep = 0;
						if (!webview.CanGoBack())
						{
							backIcon.Visibility = ViewStates.Invisible;
						}
					}
					else {*/
						webview.GoBack();
						if (!webview.CanGoBack())
						{
							backIcon.Visibility = ViewStates.Invisible;
						}
					//}
						
				}

			};


			var chrome = new FileChooserWebChromeClient((uploadMsg, acceptType, capture) =>
			{
				mUploadMessage = uploadMsg;
				var i = new Intent(Intent.ActionGetContent);
				i.AddCategory(Intent.CategoryOpenable);
				i.SetType("image/*");
				StartActivityForResult(Intent.CreateChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
			});

			bar = FindViewById<ProgressBar>(Resource.Id.progressBar1);

			webview = FindViewById<WebView>(Resource.Id.webView1);
			webview.Settings.JavaScriptEnabled = true;
			//webview.Settings.SetGeolocationEnabled (true);
			//webview.Settings.SetGeolocationDatabasePath( Context.FilesDir.Path );
			//webview.Settings.AllowFileAccess = true;
			//webview.Settings.AllowContentAccess = true;
			//webview.Settings.JavaScriptCanOpenWindowsAutomatically = true;
			//webview.Settings.LoadsImagesAutomatically = true;

			webview.Settings.LoadWithOverviewMode = true;
			webview.Settings.UseWideViewPort = true;
			webview.Settings.BuiltInZoomControls = true;

			webview.SetWebViewClient(new MyWebViewClientHome());
			webview.SetWebChromeClient(chrome);
			/*webview.SetWebChromeClient (new MyOpenFileWebChromeClient(uploadMsg => {
						HomePageActivity.Instance.mUploadMessage = uploadMsg;
                        var intent = new Intent (Intent.ActionGetContent);
                        intent.AddCategory(Intent.CategoryOpenable);
                        intent.SetType("image/*");
                        StartActivityForResult(Intent.CreateChooser(intent, "File Chooser"),
                            1);
            }));*/
			//webview.SetWebChromeClient(HomePageActivity.Instance.chrome);

			webview.LoadUrl ("http://www.whatwhenwhere.it/");
			//webview.Settings.CacheMode = CacheModes.NoCache;

			/*
			//Initialize fragment
			Fragment fragment;
			//Bundle args = new Bundle();
			fragment = new FragmentHome();
			//args.putString(FragmentHome.ITEM_NAME, dataList.get(1).getItemName());
			//fragment.setArguments(args);
			FragmentManager frgManager = SupportFragmentManager;
			frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
				.Commit();

			SupportFragmentManager.BackStackChanged += delegate
			{
				Console.WriteLine("INFO" + (SupportFragmentManager.BackStackEntryCount).ToString());
				if (SupportFragmentManager.BackStackEntryCount == 0)
				{
					backIcon.Visibility = ViewStates.Invisible;
				}
				else {
					//backIcon.Visibility = ViewStates.Visible;
					//backIcon.SetImageResource(Resource.Drawable.back_icon);
				}
			};
			*/
			if (NotificationOption)
			{
				var intent = new Intent(this, typeof(FragmentSettings));
				StartActivity(intent);
				Finish();

				isSetting = true;
				isListNot = false;
			}

			if (NotificationNot)
			{
				var intent = new Intent(this, typeof(FragmentLIstNotification));
				StartActivity(intent);
				Finish();

				isSetting = false;
				isListNot = true;
			}

		}

		protected override int getLayoutResource() {
			return Resource.Layout.HomePageLayout;
		}


		protected override void OnActivityResult(int requestCode, Result resultCode, Intent intent)
		{
			if (requestCode == FILECHOOSER_RESULTCODE)
			{
				if (null == mUploadMessage)
					return;
				Java.Lang.Object result = intent == null || resultCode != Result.Ok
					? null
					: intent.Data;
				mUploadMessage.OnReceiveValue(result);
				mUploadMessage = null;
			}
		}

		public override void OnBackPressed ()
		{

			if (webview.CanGoBack())
			{
				webview.GoBack();
				if (!webview.CanGoBack())
				{
						backIcon.Visibility = ViewStates.Invisible;
				}
				Console.WriteLine("BACK:" + webview.Url);
				HomePageActivity.Instance.Lasturl = webview.Url;
			}
			else { 
					
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.SetTitle("Chiusura");
				alert.SetMessage("Vuoi chiudere l'app?");
				alert.SetPositiveButton("Chiudi", (senderAlert, args) =>
				{
					MainActivity.Instance.Fine = true;
					Finish();
				});
				alert.SetNegativeButton("Annulla", (senderAlert, args) => { });
				alert.Show();
					
			}


		}

		public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
		}

	}

	public class FileChooserWebChromeClient : WebChromeClient
	{
		Action<IValueCallback, Java.Lang.String, Java.Lang.String> callback;

		public FileChooserWebChromeClient(Action<IValueCallback, Java.Lang.String, Java.Lang.String> callback)
		{
			this.callback = callback;
		}

		public override void OnProgressChanged(WebView view, int progress)
		{
			//Console.WriteLine (progress);

			if (progress == 100)
			{
				HomePageActivity.Instance.bar.Progress = 0;
				HomePageActivity.Instance.bar.Visibility = ViewStates.Gone;

			}
			else {
				HomePageActivity.Instance.bar.Progress = progress;
				HomePageActivity.Instance.bar.Visibility = ViewStates.Visible;
				HomePageActivity.Instance.Lasturl = view.Url;
				Console.WriteLine("Progress " + progress + "%  |  Url:" + view.Url);
			}

		}

		public override void OnGeolocationPermissionsShowPrompt(string origin, GeolocationPermissions.ICallback callback2)
		{
			System.Console.WriteLine("SHOW");
			callback2.Invoke(origin, MainActivity.Instance.Location, false);
		}

		//For Android 4.1
		[Java.Interop.Export]
		public void openFileChooser(IValueCallback uploadMsg, Java.Lang.String acceptType, Java.Lang.String capture)
		{
			callback(uploadMsg, acceptType, capture);
		}
	}

	class MyWebViewClientHome : WebViewClient
	{
		Boolean loadingFinished = true;
		Boolean redirect = false;

		public MyWebViewClientHome()
		{
		}
		public override bool ShouldOverrideUrlLoading(WebView view, string url)
		{



			if (url.Contains("www.google.com"))
			{
				loadingFinished = false;
			}

			if (!loadingFinished)
			{
				redirect = true;
			}

			if (url.StartsWith("whatsapp://"))
			{
				try
				{
					var uri = new Uri(url);
					var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));
					intent.AddFlags(ActivityFlags.NewTask);
					System.Console.WriteLine("AA");
					Application.Context.StartActivity(intent);
				}
				catch (Exception)
				{
				}

				//e.Cancel = true;
			}
			else {
				loadingFinished = false;
				view.LoadUrl(url);
				HomePageActivity.Instance.Lasturl = url;
				if (!view.CanGoBack())
					HomePageActivity.Instance.backIcon.Visibility = ViewStates.Visible;
				System.Console.WriteLine("Loading web view...-->" + url);

			}
			return true;
		}

		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			base.OnPageStarted(view, url, favicon);
			loadingFinished = false;
			System.Console.WriteLine("Start Loading!!!");
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (!redirect)
			{
				System.Console.WriteLine("1");
				loadingFinished = true;
			}

			if (loadingFinished && !redirect)
			{
				System.Console.WriteLine("Finished Loading!!!");
				RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
				par.SetMargins(0, 0, 0, 0);
				view.LayoutParameters = par;//new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent,LinearLayout.LayoutParams.MatchParent);
			}
			else
			{
				System.Console.WriteLine("2");
				redirect = false;
			}
		}


	}
}

