﻿using System;
using System.Threading;

using Android.App;
using Android.Content.PM;

using Android.OS;

using Android.Content;

namespace WhatWhenWhere
{
	[Activity(Label = "WhatWhenWhere", MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashScreen : Activity
	{
		bool notification;
		string url;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.SplashScreen);

			notification = Intent.GetBooleanExtra("Notification", false);
			url = Intent.GetStringExtra("url");

			ThreadPool.QueueUserWorkItem (o => LoadActivity ());
		}

		public void LoadActivity(){
			Thread.Sleep (1000);
			RunOnUiThread (() => {
				var intent = new Intent(this, typeof(MainActivity));

				if(notification){

					intent.PutExtra("url", url);
					intent.PutExtra ("Notification",true);

				}

				//Console.WriteLine("2");

				StartActivity(intent);
				Finish();
			});
		}
	}
}

