﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Support.V4.View;
using Android.Support.V4.Widget;

namespace WhatWhenWhere
{
	[Activity(Label = "NotificationActivity", Theme = "@style/AppTheme", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize/*, ScreenOrientation = ScreenOrientation.Portrait*/)]
	public class NotificationActivity : BaseActivity
	{
		public WebView webview;
		public ProgressBar bar;

		string url;

		public ImageView menuIcon;
		public ImageView backIcon;
		public ImageView notIcon;
		public ImageView relIcon;

		public static NotificationActivity Instance { private set; get; }

		public string Lasturl = "http://www.whatwhenwhere.it/";
		public int BackStep = 0;

		protected override void OnCreate(Bundle bundle)
		{
			// Use this to return your custom view for this Fragment
			//View view = inflater.Inflate(Resource.Layout.FragmentWebView, container, false);
			base.OnCreate(bundle);

			url = Intent.GetStringExtra("url");

			Lasturl = url;

			Console.WriteLine("NOTIFICATION");

			setActionBarIcon(Resource.Drawable.settings, Resource.Drawable.back_icon ,Resource.Drawable.notifica,Resource.Drawable.reload);

			menuIcon = (ImageView)getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView)getToolbar().FindViewById(Resource.Id.back_icon);
			notIcon = (ImageView)getToolbar().FindViewById(Resource.Id.notification_icon);
			relIcon = (ImageView)getToolbar().FindViewById(Resource.Id.reload_icon);

			menuIcon.Visibility = ViewStates.Visible;

			relIcon.Click += delegate
			{
				
				webview.LoadUrl(Lasturl);

			};

			notIcon.Click += delegate
			{

				var intent = new Intent(this, typeof(HomePageActivity));
				intent.PutExtra("Not", true);
				StartActivity(intent);
				Finish();

			};

			menuIcon.Click += delegate
			{
				var intent = new Intent(this, typeof(HomePageActivity));
				intent.PutExtra("Option", true);
				StartActivity(intent);
				Finish();
			};
			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

			backIcon.Click += delegate
			{
				var intent = new Intent(this, typeof(HomePageActivity));
				StartActivity(intent);
				Finish();
			};

			NotificationActivity.Instance = this;
			bar = FindViewById<ProgressBar>(Resource.Id.progressBar1);

			webview = FindViewById<WebView>(Resource.Id.webView1);
			webview.Settings.JavaScriptEnabled = true;
			webview.Settings.SetGeolocationEnabled(true);
			webview.Settings.SetGeolocationDatabasePath(ApplicationContext.FilesDir.Path);

			webview.SetWebViewClient(new MyWebViewClientNotifica());

			webview.SetWebChromeClient(new MyWebViewChromeNotifica());


			webview.LoadUrl(url);
			webview.Settings.CacheMode = CacheModes.NoCache;

			RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
			par.SetMargins(0, 0, 0, 0);
			webview.LayoutParameters = par;

		}



		public int PixelsToDp(int pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}

		protected override int getLayoutResource()
		{
			return Resource.Layout.Notification;
		}

		public override void OnBackPressed()
		{
			Console.WriteLine("INFO" + (SupportFragmentManager.BackStackEntryCount).ToString());

			var intent = new Intent(this, typeof(HomePageActivity));
			StartActivity(intent);
			Finish();
		}
		public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
		}

	}

	class MyWebViewChromeNotifica : WebChromeClient
	{
		public MyWebViewChromeNotifica()
		{
		}

		public override void OnProgressChanged(WebView view, int progress)
		{
			//Console.WriteLine (progress);

			if (progress == 100)
			{
				NotificationActivity.Instance.bar.Progress = 0;
				NotificationActivity.Instance.bar.Visibility = ViewStates.Gone;
			}
			else {
				NotificationActivity.Instance.bar.Progress = progress;
				NotificationActivity.Instance.bar.Visibility = ViewStates.Visible;
			}

		}

		public override void OnGeolocationPermissionsShowPrompt(string origin, GeolocationPermissions.ICallback callback)
		{
			Console.WriteLine("SHOW");
			callback.Invoke(origin, MainActivity.Instance.Location, false);
		}



	}

	class MyWebViewClientNotifica : WebViewClient
	{
		Boolean loadingFinished = true;
		Boolean redirect = false;

		public MyWebViewClientNotifica()
		{
		}
		public override bool ShouldOverrideUrlLoading(WebView view, string url)
		{
			if (!loadingFinished)
			{
				redirect = true;
			}
			if (url.StartsWith("whatsapp://"))
			{
				try
				{
					var uri = new Uri(url);
					var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));
					intent.AddFlags(ActivityFlags.NewTask);
					System.Console.WriteLine("AA");
					Application.Context.StartActivity(intent);
				}
				catch (Exception)
				{
				}

				//e.Cancel = true;
			}
			else {
				loadingFinished = false;
				view.LoadUrl(url);
				NotificationActivity.Instance.Lasturl = url;
				//HomePageActivity.Instance.backIcon.Visibility = ViewStates.Visible;
				Console.WriteLine("Loading web view...");
			}
			return true;
		}

		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			base.OnPageStarted(view, url, favicon);
			loadingFinished = false;
			Console.WriteLine("Start Loading!!!");
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (!redirect)
			{
				Console.WriteLine("1");
				loadingFinished = true;
			}

			if (loadingFinished && !redirect)
			{
				Console.WriteLine("Finished Loading!!!");
				RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
				par.SetMargins(0, 0, 0, 0);
				view.LayoutParameters = par;//new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent,LinearLayout.LayoutParams.MatchParent);
			}
			else
			{
				Console.WriteLine("2");
				redirect = false;
			}
		}


	}

}

