﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;
using Android.Widget;

using Android.Support.V7.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using RestSharp;

namespace WhatWhenWhere
{		
	[Activity(Label = "FragmentSettings", Theme = "@style/AppTheme", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize)]
	public class FragmentSettings : BaseActivity
	{

		public ImageView menuIcon;
		public ImageView backIcon;
		public ImageView notIcon;
		public ImageView relIcon;

		SwitchCompat SportSwitch,TorneiSwitch,RaduniSwitch,MostreSwitch,SfilateSwitch;
		SwitchCompat VitaSwitch,AperitiviSwitch,GaySwitch,OtakuSwitch,ConcertiSwitch;
		SwitchCompat MercatiniSwitch,FiereSwitch,SagreSwitch,InaugurazioniSwitch,CinemaSwitch;
		SwitchCompat TeatroSwitch,OnlusSwitch,ReligioneSwitch,BambiniSwitch,ItinerariSwitch;
		SwitchCompat CorsiSwitch,PromoSwitch,ScioperiSwitch,PropagandaSwitch;
		SwitchCompat LocationSwitch;

		bool SportNot, TorneiNot, RaduniNot, MostreNot, SfilateNot;
		bool VitaNot, AperitiviNot, GayNot, OtakuNot, ConcertiNot;
		bool MercatiniNot, FiereNot, SagreNot, InaugurazioniNot, CinemaNot;
		bool TeatroNot, OnlusNot, ReligioneNot, BambiniNot, ItinerariNot;
		bool CorsiNot, PromoNot, ScioperiNot, PropagandaNot;
		bool LocationEnabled,LocationEnabledAfter;

		Button SalvaButton;

		ISharedPreferences prefs;

		public static FragmentSettings Instance { private set; get;}

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			setActionBarIcon(Resource.Drawable.settings, Resource.Drawable.back_icon, Resource.Drawable.notifica, Resource.Drawable.reload);


			menuIcon = (ImageView)getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView)getToolbar().FindViewById(Resource.Id.back_icon);
			notIcon = (ImageView)getToolbar().FindViewById(Resource.Id.notification_icon);
			relIcon = (ImageView)getToolbar().FindViewById(Resource.Id.reload_icon);

			relIcon.Visibility = ViewStates.Invisible;
			menuIcon.Visibility = ViewStates.Invisible;

			notIcon.Click += delegate
			{
				var intent = new Intent(this, typeof(FragmentLIstNotification));
				StartActivity(intent);
				Finish();

			};

			backIcon.Click += delegate
			{

				var intent = new Intent(this, typeof(HomePageActivity));
				StartActivity(intent);
				Finish();

			};

			Typeface tf = Typeface.CreateFromAsset(Assets,"fonts/Lato.ttf"); 

			HomePageActivity.Instance.LastPg = 1;

			prefs = MainActivity.Instance.prefs;

			LocationEnabled=prefs.GetBoolean("LocationEnabled",false);
			LocationEnabledAfter = LocationEnabled;

			SportNot=prefs.GetBoolean("WWWSport",false);
			TorneiNot = prefs.GetBoolean("WWWTornei",false);
			RaduniNot = prefs.GetBoolean("WWWRaduni",false);
			MostreNot = prefs.GetBoolean("WWWMostre",false);
			SfilateNot = prefs.GetBoolean("WWWSfilate",false);
			VitaNot = prefs.GetBoolean("WWWVita",false);
			AperitiviNot = prefs.GetBoolean("WWWAperitivi",false);
			GayNot = prefs.GetBoolean("WWWGay",false);
			OtakuNot = prefs.GetBoolean("WWWOtaku",false);
			ConcertiNot = prefs.GetBoolean("WWWConcerti",false);
			MercatiniNot = prefs.GetBoolean("WWWMercatini",false);
			FiereNot = prefs.GetBoolean("WWWFiere",false);
			SagreNot = prefs.GetBoolean("WWWSagre",false);
			InaugurazioniNot = prefs.GetBoolean("WWWInaugurazione",false);
			CinemaNot = prefs.GetBoolean("WWWCinema",false);
			TeatroNot = prefs.GetBoolean("WWWTeatro",false);
			OnlusNot = prefs.GetBoolean("WWWOnlus",false);
			ReligioneNot = prefs.GetBoolean("WWWReligione",false);
			BambiniNot = prefs.GetBoolean("WWWBambini",false);
			ItinerariNot = prefs.GetBoolean("WWWItinerari",false);
			CorsiNot = prefs.GetBoolean("WWWCorsi",false);
			PromoNot = prefs.GetBoolean("WWWPromo",false);
			ScioperiNot = prefs.GetBoolean("WWWScioperi",false);
			PropagandaNot = prefs.GetBoolean("WWWPropaganda",false);

			SalvaButton = FindViewById<Button> (Resource.Id.SalvaButton);
			SalvaButton.Click += delegate {

				if(LocationEnabled != LocationEnabledAfter){
					if(LocationEnabledAfter){
						Console.WriteLine("sss");
						LocManager.StartLocationService();
						MainActivity.Instance.Location =LocationEnabledAfter;
						LocationEnabled=LocationEnabledAfter;
						var prefEditor2 = prefs.Edit();
						prefEditor2.PutBoolean("LocationEnabled",LocationEnabledAfter);
						prefEditor2.Commit();
					}else{
						Console.WriteLine("aaaa");
						LocManager.StopLocationService();
						MainActivity.Instance.Location =LocationEnabledAfter;
						LocationEnabled=LocationEnabledAfter;
						var prefEditor2 = prefs.Edit();
						prefEditor2.PutBoolean("LocationEnabled",LocationEnabledAfter);
						prefEditor2.Commit();
					}
				}

				var prefEditor = prefs.Edit();
				prefEditor.PutBoolean("WWWSport",SportNot);
				prefEditor.PutBoolean("WWWTornei",TorneiNot);
				prefEditor.PutBoolean("WWWRaduni",RaduniNot);
				prefEditor.PutBoolean("WWWMostre",MostreNot);
				prefEditor.PutBoolean("WWWSfilate",SfilateNot);
				prefEditor.PutBoolean("WWWVita",VitaNot);
				prefEditor.PutBoolean("WWWAperitivi",AperitiviNot);
				prefEditor.PutBoolean( "WWWGay",GayNot);
				prefEditor.PutBoolean("WWWOtaku",OtakuNot);
				prefEditor.PutBoolean("WWWConcerti",ConcertiNot);
				prefEditor.PutBoolean("WWWMercatini",MercatiniNot);
				prefEditor.PutBoolean("WWWFiere",FiereNot);
				prefEditor.PutBoolean("WWWSagre",SagreNot);
				prefEditor.PutBoolean("WWWInaugurazione",InaugurazioniNot);
				prefEditor.PutBoolean("WWWCinema",CinemaNot);
				prefEditor.PutBoolean("WWWTeatro",TeatroNot);
				prefEditor.PutBoolean("WWWOnlus",OnlusNot);
				prefEditor.PutBoolean("WWWReligione",ReligioneNot);
				prefEditor.PutBoolean("WWWBambini",BambiniNot);
				prefEditor.PutBoolean("WWWItinerari",ItinerariNot);
				prefEditor.PutBoolean("WWWCorsi",CorsiNot);
				prefEditor.PutBoolean("WWWPromo",PromoNot);
				prefEditor.PutBoolean("WWWScioperi",ScioperiNot);
				prefEditor.PutBoolean("WWWPropaganda",PropagandaNot);
				prefEditor.Commit();


					// Invio Dati Location
					Console.WriteLine("Notification Send at http://api.whatwhenwhere.it/devices/" + Android.OS.Build.Serial + "/position");
					var client = new RestClient("http://api.whatwhenwhere.it/");

					var request = new RestRequest("devices/" + Android.OS.Build.Serial + "/categories", Method.POST);
					request.AddHeader("Content-Type", "multipart/form-data");

					//JObject oJsonObject = new JObject();


					//oJsonObject.Add("os", "Android");
					//oJsonObject.Add("latitude", location.Latitude);
					//oJsonObject.Add("longitude", location.Longitude);

					request.AddParameter("os", "Android");
					request.AddParameter("token", MainActivity.Instance.PushToken);

					if(SportNot)
						request.AddParameter("categories[]", "sport");
					if (TorneiNot)
						request.AddParameter("categories[]", "tournaments");
					if (RaduniNot)
						request.AddParameter("categories[]", "gatherings");
					if (MostreNot)
						request.AddParameter("categories[]", "exhibitions");
					if (SfilateNot)
						request.AddParameter("categories[]", "parades");
					if (VitaNot)
						request.AddParameter("categories[]", "nightlife");
					if (AperitiviNot)
						request.AddParameter("categories[]", "aperitives");
					if (GayNot)
						request.AddParameter("categories[]", "gay-lgbt");
					if (OtakuNot)
						request.AddParameter("categories[]", "otaku");
					if (ConcertiNot)
						request.AddParameter("categories[]", "concerts");
					if (MercatiniNot)
						request.AddParameter("categories[]", "markets");
					if (FiereNot)
						request.AddParameter("categories[]", "fairs");
					if (SagreNot)
						request.AddParameter("categories[]", "festivals");
					if (InaugurazioniNot)
						request.AddParameter("categories[]", "inaugurations");
					if (CinemaNot)
						request.AddParameter("categories[]", "cinema");
					if (TeatroNot)
						request.AddParameter("categories[]", "theatre");
					if (OnlusNot)
						request.AddParameter("categories[]", "onlus");
					if (ReligioneNot)
						request.AddParameter("categories[]", "religion");
					if (BambiniNot)
						request.AddParameter("categories[]", "kids");
					if (ItinerariNot)
						request.AddParameter("categories[]", "routes");
					if (CorsiNot)
						request.AddParameter("categories[]", "training-courses");
					if (PromoNot)
						request.AddParameter("categories[]", "promos-discounts");
					if (ScioperiNot)
						request.AddParameter("categories[]", "strike");
					if (PropagandaNot)
						request.AddParameter("categories[]", "propaganda");

				//requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

				foreach (var p in request.Parameters) {
					Console.WriteLine(p.Name+"|"+p.ContentType+"|"+p.Value+"|"+p.Type);
				}

					client.ExecuteAsync(request, (s, e) =>
					{

						Console.WriteLine("Result:" + s.StatusCode + "|" + s.Content);

					});

					Console.WriteLine(1);



				Toast.MakeText(ApplicationContext,"Impostazioni salvate",ToastLength.Short).Show();
			};

			FindViewById<TextView> (Resource.Id.Location).Typeface = tf;
			LocationSwitch = FindViewById<SwitchCompat> (Resource.Id.LocationSwitch);
			LocationSwitch.Checked = LocationEnabled;
			LocationSwitch.CheckedChange+= delegate {
				if(LocationSwitch.Checked){
					LocationEnabledAfter = true;
				}
				else{
					LocationEnabledAfter = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Sport).Typeface = tf;
			SportSwitch = FindViewById<SwitchCompat> (Resource.Id.SportSwitch);
			SportSwitch.Checked = SportNot;
			SportSwitch.CheckedChange+= delegate {
				if(SportSwitch.Checked){
					SportNot = true;
				}
				else{
					SportNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Tornei).Typeface = tf;
			TorneiSwitch = FindViewById<SwitchCompat> (Resource.Id.TorneiSwitch);
			TorneiSwitch.Checked = TorneiNot;
			TorneiSwitch.CheckedChange+= delegate {
				if(TorneiSwitch.Checked){
					TorneiNot = true;
				}
				else{
					TorneiNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Raduni).Typeface = tf;
			RaduniSwitch = FindViewById<SwitchCompat> (Resource.Id.RaduniSwitch);
			RaduniSwitch.Checked = RaduniNot;
			RaduniSwitch.CheckedChange+= delegate {
				if(RaduniSwitch.Checked){
					RaduniNot = true;
				}
				else{
					RaduniNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Mostre).Typeface = tf;
			MostreSwitch = FindViewById<SwitchCompat> (Resource.Id.MostreSwitch);
			MostreSwitch.Checked = MostreNot;
			MostreSwitch.CheckedChange+= delegate {
				if(MostreSwitch.Checked){
					MostreNot = true;
				}
				else{
					MostreNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Sfilate).Typeface = tf;
			SfilateSwitch = FindViewById<SwitchCompat> (Resource.Id.SfilateSwitch);
			SfilateSwitch.Checked = SfilateNot;
			SfilateSwitch.CheckedChange+= delegate {
				if(SfilateSwitch.Checked){
					SfilateNot = true;
				}
				else{
					SfilateNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Vita).Typeface = tf;
			VitaSwitch = FindViewById<SwitchCompat> (Resource.Id.VitaSwitch);
			VitaSwitch.Checked = VitaNot;
			VitaSwitch.CheckedChange+= delegate {
				if(VitaSwitch.Checked){
					VitaNot = true;
				}
				else{
					VitaNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Aperitivi).Typeface = tf;
			AperitiviSwitch = FindViewById<SwitchCompat> (Resource.Id.AperitiviSwitch);
			AperitiviSwitch.Checked = AperitiviNot;
			AperitiviSwitch.CheckedChange+= delegate {
				if(AperitiviSwitch.Checked){
					AperitiviNot = true;
				}
				else{
					AperitiviNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Gay).Typeface = tf;
			GaySwitch = FindViewById<SwitchCompat> (Resource.Id.GaySwitch);
			GaySwitch.Checked = GayNot;
			GaySwitch.CheckedChange+= delegate {
				if(GaySwitch.Checked){
					GayNot = true;
				}
				else{
					GayNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Otaku).Typeface = tf;
			OtakuSwitch = FindViewById<SwitchCompat> (Resource.Id.OtakuSwitch);
			OtakuSwitch.Checked = OtakuNot;
			OtakuSwitch.CheckedChange+= delegate {
				if(OtakuSwitch.Checked){
					OtakuNot = true;
				}
				else{
					OtakuNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Concerti).Typeface = tf;
			ConcertiSwitch = FindViewById<SwitchCompat> (Resource.Id.ConcertiSwitch);
			ConcertiSwitch.Checked = ConcertiNot;
			ConcertiSwitch.CheckedChange+= delegate {
				if(ConcertiSwitch.Checked){
					ConcertiNot = true;
				}
				else{
					ConcertiNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Mercatini).Typeface = tf;
			MercatiniSwitch = FindViewById<SwitchCompat> (Resource.Id.MercatiniSwitch);
			MercatiniSwitch.Checked = MercatiniNot;
			MercatiniSwitch.CheckedChange+= delegate {
				if(MercatiniSwitch.Checked){
					MercatiniNot = true;
				}
				else{
					MercatiniNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Fiere).Typeface = tf;
			FiereSwitch = FindViewById<SwitchCompat> (Resource.Id.FiereSwitch);
			FiereSwitch.Checked = FiereNot;
			FiereSwitch.CheckedChange+= delegate {
				if(FiereSwitch.Checked){
					FiereNot = true;
				}
				else{
					FiereNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Sagre).Typeface = tf;
			SagreSwitch = FindViewById<SwitchCompat> (Resource.Id.SagreSwitch);
			SagreSwitch.Checked = SagreNot;
			SagreSwitch.CheckedChange+= delegate {
				if(SagreSwitch.Checked){
					SagreNot = true;
				}
				else{
					SagreNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Inaugurazioni).Typeface = tf;
			InaugurazioniSwitch = FindViewById<SwitchCompat> (Resource.Id.InaugurazioniSwitch);
			InaugurazioniSwitch.Checked = InaugurazioniNot;
			InaugurazioniSwitch.CheckedChange+= delegate {
				if(InaugurazioniSwitch.Checked){
					InaugurazioniNot = true;
				}
				else{
					InaugurazioniNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Cinema).Typeface = tf;
			CinemaSwitch = FindViewById<SwitchCompat> (Resource.Id.CinemaSwitch);
			CinemaSwitch.Checked = CinemaNot;
			CinemaSwitch.CheckedChange+= delegate {
				if(CinemaSwitch.Checked){
					CinemaNot = true;
				}
				else{
					CinemaNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Teatro).Typeface = tf;
			TeatroSwitch = FindViewById<SwitchCompat> (Resource.Id.TeatroSwitch);
			TeatroSwitch.Checked = TeatroNot;
			TeatroSwitch.CheckedChange+= delegate {
				if(TeatroSwitch.Checked){
					TeatroNot = true;
				}
				else{
					TeatroNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Onlus).Typeface = tf;
			OnlusSwitch = FindViewById<SwitchCompat> (Resource.Id.OnlusSwitch);
			OnlusSwitch.Checked = OnlusNot;
			OnlusSwitch.CheckedChange+= delegate {
				if(OnlusSwitch.Checked){
					OnlusNot = true;
				}
				else{
					OnlusNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Religione).Typeface = tf;
			ReligioneSwitch = FindViewById<SwitchCompat> (Resource.Id.ReligioneSwitch);
			ReligioneSwitch.Checked = ReligioneNot;
			ReligioneSwitch.CheckedChange+= delegate {
				if(ReligioneSwitch.Checked){
					ReligioneNot = true;
				}
				else{
					ReligioneNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Bambini).Typeface = tf;
			BambiniSwitch = FindViewById<SwitchCompat> (Resource.Id.BambiniSwitch);
			BambiniSwitch.Checked = BambiniNot;
			BambiniSwitch.CheckedChange+= delegate {
				if(BambiniSwitch.Checked){
					BambiniNot = true;
				}
				else{
					BambiniNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Itinerari).Typeface = tf;
			ItinerariSwitch = FindViewById<SwitchCompat> (Resource.Id.ItinerariSwitch);
			ItinerariSwitch.Checked = ItinerariNot;
			ItinerariSwitch.CheckedChange+= delegate {
				if(ItinerariSwitch.Checked){
					ItinerariNot = true;
				}
				else{
					ItinerariNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Corsi).Typeface = tf;
			CorsiSwitch = FindViewById<SwitchCompat> (Resource.Id.CorsiSwitch);
			CorsiSwitch.Checked = CorsiNot;
			CorsiSwitch.CheckedChange+= delegate {
				if(CorsiSwitch.Checked){
					CorsiNot = true;
				}
				else{
					CorsiNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Promo).Typeface = tf;
			PromoSwitch = FindViewById<SwitchCompat> (Resource.Id.PromoSwitch);
			PromoSwitch.Checked = PromoNot;
			PromoSwitch.CheckedChange+= delegate {
				if(PromoSwitch.Checked){
					PromoNot = true;
				}
				else{
					PromoNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Scioperi).Typeface = tf;
			ScioperiSwitch = FindViewById<SwitchCompat> (Resource.Id.ScioperiSwitch);
			ScioperiSwitch.Checked = ScioperiNot;
			ScioperiSwitch.CheckedChange+= delegate {
				if(ScioperiSwitch.Checked){
					ScioperiNot = true;
				}
				else{
					ScioperiNot = false;
				}
			};

			FindViewById<TextView> (Resource.Id.Propaganda).Typeface = tf;
			PropagandaSwitch = FindViewById<SwitchCompat> (Resource.Id.PropagandaSwitch);
			PropagandaSwitch.Checked = PromoNot;
			PropagandaSwitch.CheckedChange+= delegate {
				if(PropagandaSwitch.Checked){
					PropagandaNot = true;
				}
				else{
					PropagandaNot = false;
				}
			};
		}

		public override void OnBackPressed()
		{

			var intent = new Intent(this, typeof(HomePageActivity));
			StartActivity(intent);
			Finish();

		}

		public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
		}

		protected override int getLayoutResource()
		{
			return Resource.Layout.FragmentSettings;
		}
	}
}

