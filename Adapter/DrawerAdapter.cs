﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WhatWhenWhere
{
	public class DrawerAdapter : BaseAdapter
	{
		Activity context;

		public List<ListObject> items;

		public DrawerAdapter(Activity context) : base()
		{
			this.context = context;

			//For demo purposes we hard code some data here
			this.items = new List<ListObject>() {
				new ListObject() { First = true},
				new ListObject() { Text = "Home"},
				new ListObject() { Text = "Contatti"},
				new ListObject() { Text = "Chi Siamo"},
				new ListObject() { Text = "Impostazioni"}
			};
		}

		public override int Count
		{
			get { return items.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  

			View view = convertView;


			if(position == 0){
				LayoutInflater inflater = ((Activity) context).LayoutInflater;

				view = inflater.Inflate(Resource.Layout.drawer_logo, parent, false);
				//((ImageView) view.FindViewById<ImageView<(R.id.LogoImage)).setImageDrawable(view.getResources().getDrawable(R.drawable.logo));

				return view;

			}else {

				LayoutInflater inflater = ((Activity) context).LayoutInflater;

				ListObject dItem = this.items[position];

				view = inflater.Inflate(Resource.Layout.drawer_option, parent, false);

				view.FindViewById<TextView>(Resource.Id.optionText).Text=dItem.Text;

				return view;
			}
			/*
			//Try to reuse convertView if it's not  null, otherwise inflate it from our item layout
			// This gives us some performance gains by not always inflating a new view
			// This will sound familiar to MonoTouch developers with UITableViewCell.DequeueReusableCell()
			var view = (convertView ??
				context.LayoutInflater.Inflate(
					Resource.layout.customlistitem,
					parent,
					false)) as LinearLayout;

			//Find references to each subview in the list item's view
			var imageItem = view.FindViewById(Resource.id.imageItem) as ImageView;
			var textTop = view.FindViewById(Resource.id.textTop) as TextView;
			var textBottom = view.FindViewById(Resource.id.textBottom) as TextView;

			//Assign this item's values to the various subviews
			imageItem.SetImageResource(item.Image);
			textTop.SetText(item.Name, TextView.BufferType.Normal);
			textBottom.SetText(item.Description, TextView.BufferType.Normal);

			//Finally return the view*/
			return view;
		}

		public ListObject GetItemAtPosition(int position)
		{
			return items[position];
		}
	}


	public class ListObject
	{

		public string Text
		{
			get;
			set;
		}

		public bool First
		{
			get;
			set;
		}
	}
}

