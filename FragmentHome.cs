﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Provider;
using Java.IO;


using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Java.Text;

namespace WhatWhenWhere
{
	public class FragmentHome : Fragment
	{
		public WebView webview;
		public ProgressBar bar;

		public static FragmentHome Instance { private set; get;}

		private IValueCallback mUploadMessage;
		private static int FILECHOOSER_RESULTCODE = 1;

		public override void OnActivityResult(int requestCode, int resultCode, Intent data)
		{

			if (requestCode == FILECHOOSER_RESULTCODE)
			{

				if (null == this.mUploadMessage)
				{
					return;
				}

				Android.Net.Uri result = null;

				try
				{
					if (resultCode != int.Parse(Result.Ok.ToString()))
					{
						result = null;
					}
					else {
						// retrieve from the private variable if the intent is null
						result = data == null ? null : data.Data;
					}
				}
				catch (Exception e)
				{
					Toast.MakeText(Activity.ApplicationContext, "activity :" + e, ToastLength.Long).Show();
				}

				mUploadMessage.OnReceiveValue(result);
				mUploadMessage = null;
			}
		
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentWebView, container, false);

			FragmentHome.Instance = this;
			bar = view.FindViewById<ProgressBar> (Resource.Id.progressBar1);

			webview = view.FindViewById<WebView> (Resource.Id.webView1);
			webview.Settings.JavaScriptEnabled = true;
			//webview.Settings.SetGeolocationEnabled (true);
			//webview.Settings.SetGeolocationDatabasePath( Context.FilesDir.Path );
			//webview.Settings.AllowFileAccess = true;
			//webview.Settings.AllowContentAccess = true;
			//webview.Settings.JavaScriptCanOpenWindowsAutomatically = true;
			//webview.Settings.LoadsImagesAutomatically = true;

			webview.Settings.LoadWithOverviewMode =true;
			webview.Settings.UseWideViewPort = true;
			webview.Settings.BuiltInZoomControls = true;

			webview.SetWebViewClient (new MyWebViewClientHome());
			webview.SetWebChromeClient(new MyWebViewChromeHome());
			/*webview.SetWebChromeClient (new MyOpenFileWebChromeClient(uploadMsg => {
						HomePageActivity.Instance.mUploadMessage = uploadMsg;
                        var intent = new Intent (Intent.ActionGetContent);
                        intent.AddCategory(Intent.CategoryOpenable);
                        intent.SetType("image/*");
                        StartActivityForResult(Intent.CreateChooser(intent, "File Chooser"),
                            1);
            }));*/
			//webview.SetWebChromeClient(HomePageActivity.Instance.chrome);

			//webview.LoadUrl ("http://www.whatwhenwhere.it/");
			//webview.Settings.CacheMode = CacheModes.NoCache;
			webview.LoadUrl("http://www.script-tutorials.com/demos/199/index.html");

			return view;
		}

		public int PixelsToDp(int pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}

		public File createImageFile()
		{
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").Format(new Java.Util.Date());
        	String imageFileName = "img_" + timeStamp + "_";
			File storageDir = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures);
			return File.CreateTempFile(imageFileName,".jpg",storageDir);
    	}



	}

	class MyWebViewChromeHome : WebChromeClient
	{

		Action<IValueCallback, Java.Lang.String, Java.Lang.String> callback2;

		/*public MyWebViewChromeHome(Action<IValueCallback, Java.Lang.String, Java.Lang.String> c){
			callback2=c;
		}*/

		public MyWebViewChromeHome()
		{
			

		}

		public override void OnProgressChanged (WebView view, int progress)
		{
			//Console.WriteLine (progress);

			if (progress == 100) {
				FragmentHome.Instance.bar.Progress = 0;
				FragmentHome.Instance.bar.Visibility = ViewStates.Gone;
			} else {
				FragmentHome.Instance.bar.Progress = progress;
				FragmentHome.Instance.bar.Visibility = ViewStates.Visible;
			}

		}

		public override void OnGeolocationPermissionsShowPrompt (string origin, GeolocationPermissions.ICallback callback)
		{
			System.Console.WriteLine("SHOW");
			callback.Invoke(origin, MainActivity.Instance.Location, false);
		}

		//For Android 3.0+
		[Java.Interop.Export]
		public void openFileChooser(IValueCallback uploadMsg)
		{
			System.Console.WriteLine("OPEN FILE CHOOSER 1");

			HomePageActivity.Instance.mUM = uploadMsg;
			Intent i = new Intent(Intent.ActionGetContent);
			i.AddCategory(Intent.CategoryOpenable);
			i.SetType("image/*");
			HomePageActivity.Instance.StartActivityForResult(
				Intent.CreateChooser(i, "File Chooser"),
				1);
		}

		// For Android 3.0+, above method not supported in some android 3+ versions, in such case we use this
		[Java.Interop.Export]
		public void openFileChooser(IValueCallback uploadMsg, String acceptType)
		{
			System.Console.WriteLine("OPEN FILE CHOOSER 2");

			HomePageActivity.Instance.mUM = uploadMsg;
			Intent i = new Intent(Intent.ActionGetContent);
			i.AddCategory(Intent.CategoryOpenable);
			i.SetType("*/*");
			HomePageActivity.Instance.StartActivityForResult(
					Intent.CreateChooser(i, "File Browser"),
					1);
		}

		//For Android 4.1
		[Java.Interop.Export]
		public void openFileChooser(IValueCallback uploadMsg, String acceptType, String capture)
		{
			System.Console.WriteLine("OPEN FILE CHOOSER 3");

			HomePageActivity.Instance.mUM = uploadMsg;
			Intent i = new Intent(Intent.ActionGetContent);
			i.AddCategory(Intent.CategoryOpenable);
			i.SetType("image/*");
			HomePageActivity.Instance.StartActivityForResult(
				Intent.CreateChooser(i, "File Chooser"),
				1);
		}

		[Java.Interop.Export]
		public bool onShowFileChooser(
			WebView webView, IValueCallback filePathCallback,
			WebChromeClient.ICustomViewCallback fileChooserParams)
		{

			System.Console.WriteLine("ON SHOW FILE CHOOSER");

			if (HomePageActivity.Instance.mUMA != null)
			{
				HomePageActivity.Instance.mUMA.OnReceiveValue(null);
			}
			HomePageActivity.Instance.mUMA = filePathCallback;
			Intent takePictureIntent = new Intent(MediaStore.ActionImageCapture);
			if (takePictureIntent.ResolveActivity(MainActivity.Instance.PackageManager) != null)
			{
				File photoFile = null;
				try
				{
					photoFile = FragmentHome.Instance.createImageFile();
					takePictureIntent.PutExtra("PhotoPath",HomePageActivity.Instance.mCM);
				}
				catch (IOException ex)
				{
					Log.Error("APP", "Image file creation failed", ex);
				}
				if (photoFile != null)
				{
					HomePageActivity.Instance.mCM = "file:" + photoFile.AbsolutePath;
					takePictureIntent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(photoFile));
				}
				else {
					takePictureIntent = null;
				}
			}
			Intent contentSelectionIntent = new Intent(Intent.ActionGetContent);
			contentSelectionIntent.AddCategory(Intent.CategoryOpenable);
			contentSelectionIntent.SetType("image/*");
			Intent[] intentArray;
			if (takePictureIntent != null)
			{
				intentArray = new Intent[] { takePictureIntent };
			}
			else {
				intentArray = new Intent[0];
			}

			Intent chooserIntent = new Intent(Intent.ActionChooser);
			chooserIntent.PutExtra(Intent.ExtraIntent, contentSelectionIntent);
			chooserIntent.PutExtra(Intent.ExtraIntent, "Image Chooser");
			chooserIntent.PutExtra(Intent.ExtraInitialIntents, intentArray);
			HomePageActivity.Instance.StartActivityForResult(chooserIntent, 1);
			return true;
		}


	}	
		
	class MyWebViewClientHomeVecchio : WebViewClient
	{
		Boolean loadingFinished = true;
		Boolean redirect = false;

		public MyWebViewClientHomeVecchio(){
		}
		public override bool ShouldOverrideUrlLoading(WebView view, string url)
		{



			if (url.Contains("www.google.com"))
			{
				loadingFinished = false;
			}

			if (!loadingFinished)
			{
				redirect = true;
			}
				
			if (url.StartsWith("whatsapp://"))
			{
				try
				{
					var uri = new Uri(url);
					var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));
					intent.AddFlags(ActivityFlags.NewTask);
					System.Console.WriteLine("AA");
					Application.Context.StartActivity(intent);
				}
				catch (Exception)
				{
				}

				//e.Cancel = true;
			}
			else {
				loadingFinished = false;
				view.LoadUrl(url);
				HomePageActivity.Instance.Lasturl = url;
				if(!view.CanGoBackOrForward(-1-HomePageActivity.Instance.BackStep))
					HomePageActivity.Instance.backIcon.Visibility = ViewStates.Visible;
				System.Console.WriteLine("Loading web view...-->"+url);

			}
			return true;
		}

		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			base.OnPageStarted(view, url, favicon);
			loadingFinished = false;
			System.Console.WriteLine("Start Loading!!!");
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (!redirect)
			{
				System.Console.WriteLine("1");
				loadingFinished = true;
			}

			if (loadingFinished && !redirect ) 
			{
				System.Console.WriteLine("Finished Loading!!!");
				RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent,RelativeLayout.LayoutParams.MatchParent);
				par.SetMargins (0, 0, 0, 0);
				view.LayoutParameters = par;//new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent,LinearLayout.LayoutParams.MatchParent);
			}
			else
			{
				System.Console.WriteLine("2");
				redirect = false;
			}
		}
			

	}

}

