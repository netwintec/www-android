﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Webkit;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace WhatWhenWhere
{		
	public class FragmentContatti : Fragment
	{
		public WebView webview;
		public ProgressBar bar;

		public static FragmentContatti Instance { private set; get;}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentWebView, container, false);

			FragmentContatti.Instance = this;

			bar = view.FindViewById<ProgressBar> (Resource.Id.progressBar1);

			webview = view.FindViewById<WebView> (Resource.Id.webView1);
			webview.Settings.JavaScriptEnabled = true;
			webview.Settings.SetGeolocationEnabled (true);
			webview.Settings.SetGeolocationDatabasePath( Context.FilesDir.Path );
			webview.SetWebViewClient (new MyWebViewClientContatti());
			webview.SetWebChromeClient (new MyWebViewChromeContatti ());
			webview.LoadUrl ("http://www.whatwhenwhere.it/contacts.php");
			webview.Settings.CacheMode = CacheModes.CacheElseNetwork;



			return view;
		}
		public int PixelsToDp(int pixels)
		{
			return (int)Android.Util.TypedValue.ApplyDimension(Android.Util.ComplexUnitType.Dip, pixels, Resources.DisplayMetrics);
		}
	}

	class MyWebViewChromeContatti : WebChromeClient
	{
		public MyWebViewChromeContatti(){
		}
		public override void OnProgressChanged (WebView view, int progress)
		{
			//Console.WriteLine (progress);

			if (progress == 100) {
				FragmentContatti.Instance.bar.Progress = 0;
				FragmentContatti.Instance.bar.Visibility = ViewStates.Gone;
			} else {
				FragmentContatti.Instance.bar.Progress = progress;
				FragmentContatti.Instance.bar.Visibility = ViewStates.Visible;
			}

		}

		public override void OnGeolocationPermissionsShowPrompt (string origin, GeolocationPermissions.ICallback callback)
		{
			Console.WriteLine("SHOW");
			callback.Invoke(origin, MainActivity.Instance.Location, false);
		}

	}

	class MyWebViewClientContatti : WebViewClient
	{
		Boolean loadingFinished = true;
		Boolean redirect = false;

		public MyWebViewClientContatti(){
		}
		public override bool ShouldOverrideUrlLoading(WebView view, string url)
		{

			if (url.Contains ("http://www.whatwhenwhere.it/contacts.php")) {
				if (!loadingFinished) {
					redirect = true;
				}

				loadingFinished = false;
				view.LoadUrl (url);
				//HomePageActivity.Instance.backIcon.Visibility = ViewStates.Visible;
				Console.WriteLine ("Loading web view...");
				return true;
			} else {

				view.StopLoading ();
				return false;

			}
		}

		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			base.OnPageStarted(view, url, favicon);
			loadingFinished = false;
			Console.WriteLine("Start Loading!!!");
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (!redirect)
			{
				Console.WriteLine("1");
				loadingFinished = true;
			}

			if (loadingFinished && !redirect) 
			{
				Console.WriteLine("Finished Loading!!!");
				RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent,RelativeLayout.LayoutParams.MatchParent);
				par.SetMargins (0, - FragmentContatti.Instance.PixelsToDp(50), 0, 0);
				view.LayoutParameters = par;//new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent,LinearLayout.LayoutParams.MatchParent);
			}
			else
			{
				Console.WriteLine("2");
				redirect = false;
			}
		}

	}
}

